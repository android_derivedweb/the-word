package com.example.theword.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.theword.Model.VersesInnerModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView versesPage, about_us, contact_us, favourite, setting, donate;
    private TextView textSentence, writer_name;
    private ImageView wordImage;

    private LinearLayout layOutMain;
    private LinearLayout layOutBottom;

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(MainActivity.this);

        versesPage = findViewById(R.id.versesPage);
        about_us = findViewById(R.id.about_us);
        contact_us = findViewById(R.id.contact_us);
        favourite = findViewById(R.id.favourite);
        setting = findViewById(R.id.setting);
        donate = findViewById(R.id.donate);
        wordImage = findViewById(R.id.wordImage);
        textSentence = findViewById(R.id.textSentence);
        layOutMain = findViewById(R.id.layOutMain);
        layOutBottom = findViewById(R.id.layOutBottom);
        writer_name = findViewById(R.id.writer_name);



        versesPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, VersesPage.class));
            }
        });
        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, About_Us.class));
            }
        });
        contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Contact_Us.class));
            }
        });
        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Favourites.class));
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Setting.class));
            }
        });
        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Donate.class));
            }
        });


    //    setDeviceToken();

        getData();

        darkMode();


        if(session.getFontstyle().equals("Helvetica")){
            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.helvetica_1), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.gill_sans_std_2), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Geograph")){

            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.zack_3), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Proforma")){
            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.proforma_4), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Calibri")){
            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.calibri_5), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Times New Roman")){
            versesPage.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);
            about_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);
            contact_us.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);
            favourite.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);
            setting.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);
            donate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.times_new_roman_6), Typeface.BOLD);

        }

        FirebaseApp.initializeApp(MainActivity.this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            //          Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        session.setFirbaseDeviceToken(token);
                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.e("fireBaseToken", msg + "");

                        setDeviceToken();
                    }
                });




    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")){
            versesPage.setTextColor(getResources().getColor(R.color.orange));
            about_us.setTextColor(getResources().getColor(R.color.gray_light));
            contact_us.setTextColor(getResources().getColor(R.color.gray_light));
            favourite.setTextColor(getResources().getColor(R.color.gray_light));
            setting.setTextColor(getResources().getColor(R.color.gray_light));
            donate.setTextColor(getResources().getColor(R.color.gray_light));

            textSentence.setTextColor(getResources().getColor(R.color.white));

            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            layOutBottom.setBackgroundDrawable(getResources().getDrawable(R.drawable.dark_circle_home));
        } else {
            versesPage.setTextColor(getResources().getColor(R.color.orange));
            about_us.setTextColor(getResources().getColor(R.color.black_light));
            contact_us.setTextColor(getResources().getColor(R.color.black_light));
            favourite.setTextColor(getResources().getColor(R.color.black_light));
            setting.setTextColor(getResources().getColor(R.color.black_light));
            donate.setTextColor(getResources().getColor(R.color.black_light));

            textSentence.setTextColor(getResources().getColor(R.color.black_light));

            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            layOutBottom.setBackgroundDrawable(getResources().getDrawable(R.drawable.light_circle_home));

        }

     //   textSentence.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);

    }


    private void getData(){
        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "home-manage",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    textSentence.setText(jsonArray.getJSONObject(0).getString("home_text"));
                                    writer_name.setText(jsonArray.getJSONObject(0).getString("writer_name"));

                                    if (session.getIsdarkmode().equals("ON")){
                            //            layOutMain.setBackgroundDrawable(getResources().getDrawable(R.drawable.main_light2));
                                        /*Glide.with(MainActivity.this)
                                                .load(jsonArray.getJSONObject(0).getString("dark_mode_image_url"))
                                                .into(layOutMain);*/
                                        Glide.with(MainActivity.this).load(jsonArray.getJSONObject(0).getString("dark_mode_image_url"))
                                                .into(new SimpleTarget<Drawable>() {
                                            @Override
                                            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                                    layOutMain.setBackground(resource);
                                                }
                                            }
                                        });
                                    } else {
                                       /* Glide.with(MainActivity.this)
                                                .load(jsonArray.getJSONObject(0).getString("light_mode_image_url"))
                                                .into(layOutMain);*/
                                        Glide.with(MainActivity.this).load(jsonArray.getJSONObject(0).getString("light_mode_image_url"))
                                                .into(new SimpleTarget<Drawable>() {
                                                    @Override
                                                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                                            layOutMain.setBackground(resource);
                                                        }
                                                    }
                                                });
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(MainActivity.this).add(volleyMultipartRequest);
    }



    private void setDeviceToken() {
        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
             //   .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "device-token?device_type=" + "android" +
                        "&device_token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                //        progressDialog.dismiss();


                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                session.setUserId(object.getString("user_id"));

                                Log.e("user_id", session.getUserId() +"--");


                            }else {
                                Toast.makeText(MainActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                  //      progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(MainActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(MainActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(MainActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(MainActivity.this).add(volleyMultipartRequest);

    }



}