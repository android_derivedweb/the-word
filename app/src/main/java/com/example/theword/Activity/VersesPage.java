package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.theword.Adapter.AdapterTopics;
import com.example.theword.Model.VersesModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class VersesPage extends AppCompatActivity {

    private RecyclerView recTopicList;
    private AdapterTopics adapterTopics;

    private TextView topicMiss;
    private LinearLayout linearLay;
    private RelativeLayout mainLayout, bottomBar;
    private ImageView wordImage, searchIcon;
    private EditText editSerch;
    private TextView orderA_Z, orderZ_A,recently_addedTxt, noDataText;

    private UserSession session;

    private ArrayList<VersesModel> versesModelArrayList = new ArrayList<>();
    private String orderAZ_ZA = "";
    private String orderName = "";
    private String recently_added = "";

    private TextView A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R1, S, T, U, V, W, X, Y, Z;

    private Timer timer = new Timer();
    private final long DELAY = 1000; // milliseconds



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verses_page);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(VersesPage.this);

        topicMiss = findViewById(R.id.topicMiss);
        linearLay = findViewById(R.id.linearLay);
        mainLayout = findViewById(R.id.mainLayout);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        editSerch = findViewById(R.id.editSerch);
        searchIcon = findViewById(R.id.searchIcon);
        orderA_Z = findViewById(R.id.orderA_Z);
        orderZ_A = findViewById(R.id.orderZ_A);
        recently_addedTxt = findViewById(R.id.recently_addedTxt);
        noDataText = findViewById(R.id.noDataText);



        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VersesPage.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.orderA_Z).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderAZ_ZA = "a-z";
                orderName = "ascending";
                getData(orderAZ_ZA, orderName, "", "", "");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });

        findViewById(R.id.orderZ_A).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderAZ_ZA = "z-a";
                orderName = "descending";
                getData(orderAZ_ZA, orderName, "", "", "");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });

        findViewById(R.id.recently_addedTxt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                versesModelArrayList.clear();
                getData("", "", "", "true", "");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });

        recTopicList = findViewById(R.id.recTopicList);
        recTopicList.setLayoutManager(new LinearLayoutManager(VersesPage.this));
        adapterTopics = new AdapterTopics(VersesPage.this, versesModelArrayList, new AdapterTopics.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                Intent intent = new Intent(VersesPage.this, VersesPageInner.class);
                intent.putExtra("topic_id", versesModelArrayList.get(item).getTopic_id());
                startActivity(intent);
            }
        });
        recTopicList.setAdapter(adapterTopics);



        editSerch.addTextChangedListener(new TextWatcher() {

            boolean isTyping = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {

                Log.d("", "");
                if(!isTyping) {
                    isTyping = true;
                }
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                isTyping = false;
                                Log.e("Chhhheckkk", "stopped typing");

                                 if (editSerch.getText().length() == 1){
                                     getData("", "", "", orderName, editSerch.getText().toString());
                                 } else {
                                     getData("", orderName, editSerch.getText().toString(), "", "");
                                 }

                                if (session.getIsdarkmode().equals("ON")) {
                                    A.setTextColor(getResources().getColor(R.color.gray_light));
                                    B.setTextColor(getResources().getColor(R.color.gray_light));
                                    C.setTextColor(getResources().getColor(R.color.gray_light));
                                    D.setTextColor(getResources().getColor(R.color.gray_light));
                                    E.setTextColor(getResources().getColor(R.color.gray_light));
                                    F.setTextColor(getResources().getColor(R.color.gray_light));
                                    G.setTextColor(getResources().getColor(R.color.gray_light));
                                    H.setTextColor(getResources().getColor(R.color.gray_light));
                                    I.setTextColor(getResources().getColor(R.color.gray_light));
                                    J.setTextColor(getResources().getColor(R.color.gray_light));
                                    K.setTextColor(getResources().getColor(R.color.gray_light));
                                    L.setTextColor(getResources().getColor(R.color.gray_light));
                                    M.setTextColor(getResources().getColor(R.color.gray_light));
                                    N.setTextColor(getResources().getColor(R.color.gray_light));
                                    O.setTextColor(getResources().getColor(R.color.gray_light));
                                    P.setTextColor(getResources().getColor(R.color.gray_light));
                                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                                    S.setTextColor(getResources().getColor(R.color.gray_light));
                                    T.setTextColor(getResources().getColor(R.color.gray_light));
                                    U.setTextColor(getResources().getColor(R.color.gray_light));
                                    V.setTextColor(getResources().getColor(R.color.gray_light));
                                    W.setTextColor(getResources().getColor(R.color.gray_light));
                                    X.setTextColor(getResources().getColor(R.color.gray_light));
                                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                                }
                                else {
                                    A.setTextColor(getResources().getColor(R.color.black_light));
                                    B.setTextColor(getResources().getColor(R.color.black_light));
                                    C.setTextColor(getResources().getColor(R.color.black_light));
                                    D.setTextColor(getResources().getColor(R.color.black_light));
                                    E.setTextColor(getResources().getColor(R.color.black_light));
                                    F.setTextColor(getResources().getColor(R.color.black_light));
                                    G.setTextColor(getResources().getColor(R.color.black_light));
                                    H.setTextColor(getResources().getColor(R.color.black_light));
                                    I.setTextColor(getResources().getColor(R.color.black_light));
                                    J.setTextColor(getResources().getColor(R.color.black_light));
                                    K.setTextColor(getResources().getColor(R.color.black_light));
                                    L.setTextColor(getResources().getColor(R.color.black_light));
                                    M.setTextColor(getResources().getColor(R.color.black_light));
                                    N.setTextColor(getResources().getColor(R.color.black_light));
                                    O.setTextColor(getResources().getColor(R.color.black_light));
                                    P.setTextColor(getResources().getColor(R.color.black_light));
                                    Q.setTextColor(getResources().getColor(R.color.black_light));
                                    R1.setTextColor(getResources().getColor(R.color.black_light));
                                    S.setTextColor(getResources().getColor(R.color.black_light));
                                    T.setTextColor(getResources().getColor(R.color.black_light));
                                    U.setTextColor(getResources().getColor(R.color.black_light));
                                    V.setTextColor(getResources().getColor(R.color.black_light));
                                    W.setTextColor(getResources().getColor(R.color.black_light));
                                    X.setTextColor(getResources().getColor(R.color.black_light));
                                    Y.setTextColor(getResources().getColor(R.color.black_light));
                                    Z.setTextColor(getResources().getColor(R.color.black_light));
                                }

                            }
                        },
                        DELAY
                );

            }
        });


        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editSerch.getText().length() == 1){
                    getData("", "", "", orderName, editSerch.getText().toString());
                } else {
                    getData("", orderName, editSerch.getText().toString(), "", "");
                }

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });


        findViewById(R.id.topicMissing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topicMissDialog();
            }
        });


        getData(orderAZ_ZA, orderName, "", "", "");

        darkMode();
        alphaBaticSearch();


        if(session.getFontstyle().equals("Helvetica")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.helvetica_1), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.helvetica_1), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.helvetica_1), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.gill_sans_std_2), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.gill_sans_std_2), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Geograph")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.zack_3), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.zack_3), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.zack_3), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.zack_3), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Proforma")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.proforma_4), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.proforma_4), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.proforma_4), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.calibri_5), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.calibri_5), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.calibri_5), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            orderA_Z.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.times_new_roman_6), Typeface.NORMAL);
            orderZ_A.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.times_new_roman_6), Typeface.NORMAL);
            recently_addedTxt.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.times_new_roman_6), Typeface.BOLD);
            topicMiss.setTypeface(ResourcesCompat.getFont(VersesPage.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }



    }


    private void getData(String orderAZ_ZA, String orderName, String search, String recently_added, String single_letter){
        /*final KProgressHUD progressDialog = KProgressHUD.create(VersesPage.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);*/
         //       .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-topics?recently_added=" + recently_added
                        + "&" + orderName + "=" + orderAZ_ZA
                        + "&search=" + search
                        + "&single_letter=" + single_letter,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

             //           progressDialog.dismiss();
                        versesModelArrayList.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " dd");
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++){
                                        JSONObject object = jsonArray.getJSONObject(i);

                                        VersesModel versesModel = new VersesModel();
                                        versesModel.setTopic_id(object.getString("topic_id"));
                                        versesModel.setTopic(object.getString("topic"));
                                        versesModel.setTopic_small_case(object.getString("topic").toLowerCase());
                                        versesModelArrayList.add(versesModel);
                                    }

                                    adapterTopics.notifyDataSetChanged();


                                    if (versesModelArrayList.isEmpty()){
                                        noDataText.setVisibility(View.VISIBLE);
                                    } else {
                                        noDataText.setVisibility(View.GONE);
                                    }



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(VersesPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(VersesPage.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(VersesPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                 //       progressDialog.dismiss();


                        }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
           //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
           //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(VersesPage.this).add(volleyMultipartRequest);
    }


    public void filter(String text){
        ArrayList<VersesModel> temp = new ArrayList();
        for(VersesModel d: versesModelArrayList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getTopic_small_case().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        adapterTopics.updateList(temp);
    }


    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            linearLay.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            mainLayout.setBackgroundColor(getResources().getColor(R.color.background_black));

            topicMiss.setTextColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));
        } else {
            linearLay.setBackgroundColor(getResources().getColor(R.color.gray_light));
            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            mainLayout.setBackgroundColor(getResources().getColor(R.color.white));

            topicMiss.setTextColor(getResources().getColor(R.color.black_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));
        }




    }


    private void topicMissDialog(){

        final Dialog dialog = new Dialog(VersesPage.this);
        dialog.setContentView(R.layout.dialog_topic_missing);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RelativeLayout subMitButton = dialog.findViewById(R.id.subMitButton);
        EditText streetAddressSeller = dialog.findViewById(R.id.streetAddressSeller);

        subMitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!streetAddressSeller.getText().toString().isEmpty()){
                    addTopic(streetAddressSeller.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(VersesPage.this, "enter topic!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void addTopic(String topicEdit){
        final KProgressHUD progressDialog = KProgressHUD.create(VersesPage.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                session.BASEURL + "topic-missing",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    Toast.makeText(VersesPage.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(VersesPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(VersesPage.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(VersesPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("topic", topicEdit);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(VersesPage.this).add(volleyMultipartRequest);
    }


    private void alphaBaticSearch() {

        A = findViewById(R.id.A);
        B = findViewById(R.id.B);
        C = findViewById(R.id.C);
        D = findViewById(R.id.D);
        E = findViewById(R.id.E);
        F = findViewById(R.id.F);
        G = findViewById(R.id.G);
        H = findViewById(R.id.H);
        I = findViewById(R.id.I);
        J = findViewById(R.id.J);
        K = findViewById(R.id.K);
        L = findViewById(R.id.L);
        M = findViewById(R.id.M);
        N = findViewById(R.id.N);
        O = findViewById(R.id.O);
        P = findViewById(R.id.P);
        Q = findViewById(R.id.Q);
        R1 = findViewById(R.id.R1);
        S = findViewById(R.id.S);
        T = findViewById(R.id.T);
        U = findViewById(R.id.U);
        V = findViewById(R.id.V);
        W = findViewById(R.id.W);
        X = findViewById(R.id.X);
        Y = findViewById(R.id.Y);
        Z = findViewById(R.id.Z);

        findViewById(R.id.A).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "A");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.orange));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.orange));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.B).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "B");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.orange));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.orange));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.C).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "C");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.orange));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.orange));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.D).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "D");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.orange));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.orange));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.E).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "E");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.orange));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.orange));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.F).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "F");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.orange));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.orange));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.G).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "G");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.orange));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.orange));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.H).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "H");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.orange));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.orange));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.I).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "","I");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.orange));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.orange));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.J).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "J");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.orange));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.orange));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.K).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "K");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.orange));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.orange));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.L).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "L");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.orange));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.orange));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.M).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "M");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.orange));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.orange));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.N).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "N");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.orange));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.orange));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.O).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "O");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.orange));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.orange));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.P).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "P");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.orange));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.orange));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.Q).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "Q");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.orange));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.orange));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.R1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "R");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.orange));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.orange));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.S).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "S");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.orange));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.orange));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.T).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "T");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.orange));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.orange));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.U).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "U");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.orange));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.orange));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.V).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "V");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.orange));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.orange));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.W).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "W");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.orange));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.orange));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.X).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "X");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.orange));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.orange));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.Y).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "Y");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.orange));
                    Z.setTextColor(getResources().getColor(R.color.gray_light));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.orange));
                    Z.setTextColor(getResources().getColor(R.color.black_light));
                }
            }
        });
        findViewById(R.id.Z).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("", "", "", "", "Z");

                if (session.getIsdarkmode().equals("ON")) {
                    A.setTextColor(getResources().getColor(R.color.gray_light));
                    B.setTextColor(getResources().getColor(R.color.gray_light));
                    C.setTextColor(getResources().getColor(R.color.gray_light));
                    D.setTextColor(getResources().getColor(R.color.gray_light));
                    E.setTextColor(getResources().getColor(R.color.gray_light));
                    F.setTextColor(getResources().getColor(R.color.gray_light));
                    G.setTextColor(getResources().getColor(R.color.gray_light));
                    H.setTextColor(getResources().getColor(R.color.gray_light));
                    I.setTextColor(getResources().getColor(R.color.gray_light));
                    J.setTextColor(getResources().getColor(R.color.gray_light));
                    K.setTextColor(getResources().getColor(R.color.gray_light));
                    L.setTextColor(getResources().getColor(R.color.gray_light));
                    M.setTextColor(getResources().getColor(R.color.gray_light));
                    N.setTextColor(getResources().getColor(R.color.gray_light));
                    O.setTextColor(getResources().getColor(R.color.gray_light));
                    P.setTextColor(getResources().getColor(R.color.gray_light));
                    Q.setTextColor(getResources().getColor(R.color.gray_light));
                    R1.setTextColor(getResources().getColor(R.color.gray_light));
                    S.setTextColor(getResources().getColor(R.color.gray_light));
                    T.setTextColor(getResources().getColor(R.color.gray_light));
                    U.setTextColor(getResources().getColor(R.color.gray_light));
                    V.setTextColor(getResources().getColor(R.color.gray_light));
                    W.setTextColor(getResources().getColor(R.color.gray_light));
                    X.setTextColor(getResources().getColor(R.color.gray_light));
                    Y.setTextColor(getResources().getColor(R.color.gray_light));
                    Z.setTextColor(getResources().getColor(R.color.orange));

                } else {
                    A.setTextColor(getResources().getColor(R.color.black_light));
                    B.setTextColor(getResources().getColor(R.color.black_light));
                    C.setTextColor(getResources().getColor(R.color.black_light));
                    D.setTextColor(getResources().getColor(R.color.black_light));
                    E.setTextColor(getResources().getColor(R.color.black_light));
                    F.setTextColor(getResources().getColor(R.color.black_light));
                    G.setTextColor(getResources().getColor(R.color.black_light));
                    H.setTextColor(getResources().getColor(R.color.black_light));
                    I.setTextColor(getResources().getColor(R.color.black_light));
                    J.setTextColor(getResources().getColor(R.color.black_light));
                    K.setTextColor(getResources().getColor(R.color.black_light));
                    L.setTextColor(getResources().getColor(R.color.black_light));
                    M.setTextColor(getResources().getColor(R.color.black_light));
                    N.setTextColor(getResources().getColor(R.color.black_light));
                    O.setTextColor(getResources().getColor(R.color.black_light));
                    P.setTextColor(getResources().getColor(R.color.black_light));
                    Q.setTextColor(getResources().getColor(R.color.black_light));
                    R1.setTextColor(getResources().getColor(R.color.black_light));
                    S.setTextColor(getResources().getColor(R.color.black_light));
                    T.setTextColor(getResources().getColor(R.color.black_light));
                    U.setTextColor(getResources().getColor(R.color.black_light));
                    V.setTextColor(getResources().getColor(R.color.black_light));
                    W.setTextColor(getResources().getColor(R.color.black_light));
                    X.setTextColor(getResources().getColor(R.color.black_light));
                    Y.setTextColor(getResources().getColor(R.color.black_light));
                    Z.setTextColor(getResources().getColor(R.color.orange));
                }
            }
        });



        if (session.getIsdarkmode().equals("ON")) {
            A.setTextColor(getResources().getColor(R.color.gray_light));
            B.setTextColor(getResources().getColor(R.color.gray_light));
            C.setTextColor(getResources().getColor(R.color.gray_light));
            D.setTextColor(getResources().getColor(R.color.gray_light));
            E.setTextColor(getResources().getColor(R.color.gray_light));
            F.setTextColor(getResources().getColor(R.color.gray_light));
            G.setTextColor(getResources().getColor(R.color.gray_light));
            H.setTextColor(getResources().getColor(R.color.gray_light));
            I.setTextColor(getResources().getColor(R.color.gray_light));
            J.setTextColor(getResources().getColor(R.color.gray_light));
            K.setTextColor(getResources().getColor(R.color.gray_light));
            L.setTextColor(getResources().getColor(R.color.gray_light));
            M.setTextColor(getResources().getColor(R.color.gray_light));
            N.setTextColor(getResources().getColor(R.color.gray_light));
            O.setTextColor(getResources().getColor(R.color.gray_light));
            P.setTextColor(getResources().getColor(R.color.gray_light));
            Q.setTextColor(getResources().getColor(R.color.gray_light));
            R1.setTextColor(getResources().getColor(R.color.gray_light));
            S.setTextColor(getResources().getColor(R.color.gray_light));
            T.setTextColor(getResources().getColor(R.color.gray_light));
            U.setTextColor(getResources().getColor(R.color.gray_light));
            V.setTextColor(getResources().getColor(R.color.gray_light));
            W.setTextColor(getResources().getColor(R.color.gray_light));
            X.setTextColor(getResources().getColor(R.color.gray_light));
            Y.setTextColor(getResources().getColor(R.color.gray_light));
            Z.setTextColor(getResources().getColor(R.color.gray_light));

        } else {
            A.setTextColor(getResources().getColor(R.color.black_light));
            B.setTextColor(getResources().getColor(R.color.black_light));
            C.setTextColor(getResources().getColor(R.color.black_light));
            D.setTextColor(getResources().getColor(R.color.black_light));
            E.setTextColor(getResources().getColor(R.color.black_light));
            F.setTextColor(getResources().getColor(R.color.black_light));
            G.setTextColor(getResources().getColor(R.color.black_light));
            H.setTextColor(getResources().getColor(R.color.black_light));
            I.setTextColor(getResources().getColor(R.color.black_light));
            J.setTextColor(getResources().getColor(R.color.black_light));
            K.setTextColor(getResources().getColor(R.color.black_light));
            L.setTextColor(getResources().getColor(R.color.black_light));
            M.setTextColor(getResources().getColor(R.color.black_light));
            N.setTextColor(getResources().getColor(R.color.black_light));
            O.setTextColor(getResources().getColor(R.color.black_light));
            P.setTextColor(getResources().getColor(R.color.black_light));
            Q.setTextColor(getResources().getColor(R.color.black_light));
            R1.setTextColor(getResources().getColor(R.color.black_light));
            S.setTextColor(getResources().getColor(R.color.black_light));
            T.setTextColor(getResources().getColor(R.color.black_light));
            U.setTextColor(getResources().getColor(R.color.black_light));
            V.setTextColor(getResources().getColor(R.color.black_light));
            W.setTextColor(getResources().getColor(R.color.black_light));
            X.setTextColor(getResources().getColor(R.color.black_light));
            Y.setTextColor(getResources().getColor(R.color.black_light));
            Z.setTextColor(getResources().getColor(R.color.black_light));
        }



    }


}