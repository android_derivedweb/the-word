package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.theword.R;
import com.example.theword.Utils.UserSession;

public class About_Us extends AppCompatActivity {

    private TextView test ;
    private TextView desCription, textHeader;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private ScrollView scrollView;

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(About_Us.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(About_Us.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        desCription = findViewById(R.id.desCription);
        textHeader = findViewById(R.id.textHeader);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        scrollView = findViewById(R.id.scrollView);


        desCription.setText(Html.fromHtml("<p><em>Not to us,&nbsp;Lord, not to us,</em><em><br />But&nbsp;to Your name give glory,<br />Because of Your mercy, because of Your&nbsp;truth. &ndash; Psalm 115:1</em></p>\n" +
                "<p>Welcome to The Word App.</p>\n" +
                "<p>This App is designed to help you navigate the Holy Bible by topic. Discover all the Bible verses, from both the Old and New Testament, related to the topic you would like to know more about.</p>\n" +
                "<p>All scripture needs to be taken in context to be accurately understood, so we have included an explanation of the meaning and context of the verses.</p>\n" +
                "<p>We have also included an option for you to read the entire chapter in order to have a full understanding of the context.</p>\n" +
                "<p>We would like to stress that this app is <em>by no means</em> intended to replace the scriptures. We urge all our App users to read the Holy Bible for themselves.</p>\n" +
                "<p>We have used the NASB version as most scholars agree that it is one of the most accurate English translations.</p>\n" +
                "<p>If you can&rsquo;t find a topic you would like to know more about, let us know by leaving us a message. We will do our best to add it soon.</p>\n" +
                "<p>We hope that you enjoy navigating our App and we welcome all comments and feedback.</p>\n" +
                "<p>Thank you for your support and God bless.</p>"));




        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.helvetica_1), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.gill_sans_std_2), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.zack_3), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.proforma_4), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.calibri_5), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.times_new_roman_6), Typeface.BOLD);
            desCription.setTypeface(ResourcesCompat.getFont(About_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }

        darkMode();

    }

    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            desCription.setTextColor(getResources().getColor(R.color.white));
            desCription.setBackgroundColor(getResources().getColor(R.color.background_black));
            scrollView.setBackgroundColor(getResources().getColor(R.color.background_black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));
            desCription.setTextColor(getResources().getColor(R.color.black));
            desCription.setBackgroundColor(getResources().getColor(R.color.white));
            scrollView.setBackgroundColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

        }


        desCription.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));


    }


}