package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Contact_Us extends AppCompatActivity {

    private TextView textHeader, firt_name, last_name, email_address, message, submitBtn, textInfo;
    private EditText et_first_name, et_last_name, et_email_address, et_message;
    private ScrollView scrollLayout;
    private LinearLayout layOut;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private UserSession session;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Contact_Us.this);

        firt_name = findViewById(R.id.firt_name);
        last_name = findViewById(R.id.last_name);
        email_address = findViewById(R.id.email_address);
        message = findViewById(R.id.message);
        submitBtn = findViewById(R.id.submitBtn);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_email_address = findViewById(R.id.et_email_address);
        et_message = findViewById(R.id.et_message);
        scrollLayout = findViewById(R.id.scrollLayout);
        layOut = findViewById(R.id.layOut);
        textHeader = findViewById(R.id.textHeader);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        textInfo = findViewById(R.id.textInfo);

//test

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Contact_Us.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_first_name.getText().toString().isEmpty()){
                    Toast.makeText(Contact_Us.this, "enter your first name!", Toast.LENGTH_SHORT).show();
                } else if (et_last_name.getText().toString().isEmpty()){
                    Toast.makeText(Contact_Us.this, "enter your last name!", Toast.LENGTH_SHORT).show();
                } else if (et_message.getText().toString().isEmpty()){
                    Toast.makeText(Contact_Us.this, "enter message!", Toast.LENGTH_SHORT).show();
                } else if (et_email_address.getText().toString().isEmpty()){
                    Toast.makeText(Contact_Us.this, "enter email!", Toast.LENGTH_SHORT).show();
                } else if (!et_email_address.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Contact_Us.this,"Invalid email address",Toast.LENGTH_SHORT).show();
                } else {
                    contactUs();

                   /* Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "paypal.me/iskycreative"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                    intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                    startActivity(intent);*/
                }
            }
        });

        darkMode();


        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){

            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
            message.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
            message.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
            message.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.BOLD);
            firt_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
            last_name.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
            message.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
            email_address.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
            textInfo.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
            submitBtn.setTypeface(ResourcesCompat.getFont(Contact_Us.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }


    }



    private void contactUs(){
        final KProgressHUD progressDialog = KProgressHUD.create(Contact_Us.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                session.BASEURL + "contact-us?user_id=" + session.getUserId()
                + "&first_name=" + et_first_name.getText().toString()
                +"&last_name=" + et_last_name.getText().toString()
                +"&email=" + et_email_address.getText().toString()
                + "&message=" + et_message.getText().toString(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    Toast.makeText(Contact_Us.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                    finish();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(Contact_Us.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Contact_Us.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Contact_Us.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
              /*  params.put("first_name", et_first_name.getText().toString());
                params.put("last_name", et_last_name.getText().toString());
                params.put("email", et_email_address.getText().toString());
                params.put("message", et_message.getText().toString());*/
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Contact_Us.this).add(volleyMultipartRequest);
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));

            scrollLayout.setBackgroundColor(getResources().getColor(R.color.background_black));
            layOut.setBackgroundColor(getResources().getColor(R.color.black_contact_us));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            firt_name.setTextColor(getResources().getColor(R.color.white));
            last_name.setTextColor(getResources().getColor(R.color.white));
            email_address.setTextColor(getResources().getColor(R.color.white));
            message.setTextColor(getResources().getColor(R.color.white));
            submitBtn.setTextColor(getResources().getColor(R.color.white));
            submitBtn.setBackgroundColor(getResources().getColor(R.color.orange));

            et_first_name.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            et_last_name.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            et_email_address.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            et_message.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));

            et_first_name.setTextColor(getResources().getColor(R.color.white));
            et_last_name.setTextColor(getResources().getColor(R.color.white));
            et_email_address.setTextColor(getResources().getColor(R.color.white));
            et_message.setTextColor(getResources().getColor(R.color.white));

            textInfo.setTextColor(getResources().getColor(R.color.white));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));

            scrollLayout.setBackgroundColor(getResources().getColor(R.color.white));
            layOut.setBackgroundColor(getResources().getColor(R.color.gray_more_light));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            firt_name.setTextColor(getResources().getColor(R.color.black));
            last_name.setTextColor(getResources().getColor(R.color.black));
            email_address.setTextColor(getResources().getColor(R.color.black));
            message.setTextColor(getResources().getColor(R.color.black));
            submitBtn.setTextColor(getResources().getColor(R.color.white));
            submitBtn.setBackgroundColor(getResources().getColor(R.color.orange));

            et_first_name.setBackground(getResources().getDrawable(R.drawable.border_gray));
            et_last_name.setBackground(getResources().getDrawable(R.drawable.border_gray));
            et_email_address.setBackground(getResources().getDrawable(R.drawable.border_gray));
            et_message.setBackground(getResources().getDrawable(R.drawable.border_gray));

            et_first_name.setTextColor(getResources().getColor(R.color.black));
            et_last_name.setTextColor(getResources().getColor(R.color.black));
            et_email_address.setTextColor(getResources().getColor(R.color.black));
            et_message.setTextColor(getResources().getColor(R.color.black));

            textInfo.setTextColor(getResources().getColor(R.color.black_contact_us));

        }


        textInfo.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        firt_name.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        last_name.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        email_address.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        message.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        submitBtn.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

        et_first_name.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        et_last_name.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        et_email_address.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        et_message.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));


    }


}