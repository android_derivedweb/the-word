package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VersesPageDetail extends AppCompatActivity {

    private TextView read_full_chep,fullChapterText, textHeader1, textHeader2, description, textDefination, meaningText;
    private ImageView arrow;
    private LinearLayout viewSources, layOut;

    private boolean isOpen = true;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private UserSession session;

    private String verse_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verses_page_detail);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(VersesPageDetail.this);


        textHeader1 = findViewById(R.id.textHeader1);
        textHeader2 = findViewById(R.id.textHeader2);
        description = findViewById(R.id.description);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        layOut = findViewById(R.id.layOut);
        textDefination = findViewById(R.id.textDefination);
        meaningText = findViewById(R.id.meaningText);


        verse_id = getIntent().getStringExtra("verse_id");

        Log.e("vesr_id", verse_id + "--");


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VersesPageDetail.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        fullChapterText = findViewById(R.id.fullChapterText);
        read_full_chep = findViewById(R.id.read_full_chep);
        arrow = findViewById(R.id.arrow);
        viewSources = findViewById(R.id.viewSources);

        viewSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOpen){
                    arrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));
                    fullChapterText.setVisibility(View.VISIBLE);
                    isOpen = false;
                } else {
                    arrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));
                    fullChapterText.setVisibility(View.GONE);
                    isOpen = true;
                }

            }
        });

        getData(verse_id);

        darkMode();


        if(session.getFontstyle().equals("Helvetica")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.helvetica_1), Typeface.NORMAL);
           }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.gill_sans_std_2), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Geograph")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.zack_3), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Proforma")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader1.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.BOLD);
            textHeader2.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.BOLD);
            meaningText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.BOLD);
            textDefination.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.NORMAL);
            description.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.NORMAL);
            read_full_chep.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.NORMAL);
            fullChapterText.setTypeface(ResourcesCompat.getFont(VersesPageDetail.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }



    }

    private void getData(String verse_id){
        final KProgressHUD progressDialog = KProgressHUD.create(VersesPageDetail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-verse?verse_id=" + verse_id,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseDetail",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONObject object = jsonObject.getJSONObject("topic");

                                    textHeader1.setText(object.getString("topic"));


                                    JSONObject object1 = jsonObject.getJSONObject("verse");
                                    textDefination.setText(noTrailingwhiteLines(Html.fromHtml(object1.getString("description"))));

                                    textHeader2.setText(object1.getString("verse"));

                                    description.setText(noTrailingwhiteLines(Html.fromHtml(object1.getString("meaning_context"))));

                                    fullChapterText.setText(noTrailingwhiteLines(Html.fromHtml(object1.getString("chapter"))));



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(VersesPageDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(VersesPageDetail.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(VersesPageDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(VersesPageDetail.this).add(volleyMultipartRequest);
    }


    private CharSequence noTrailingwhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }


    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader1.setTextColor(getResources().getColor(R.color.gray_dark_light));
            textHeader1.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            textHeader2.setTextColor(getResources().getColor(R.color.white));
            textHeader2.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            layOut.setBackgroundColor(getResources().getColor(R.color.background_black));

            description.setTextColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            viewSources.setBackgroundResource(R.drawable.border_yello_bg_black);
            fullChapterText.setTextColor(getResources().getColor(R.color.white));
            fullChapterText.setBackgroundColor(getResources().getColor(R.color.background_black));

            textDefination.setTextColor(getResources().getColor(R.color.gray_dark_light));
            meaningText.setTextColor(getResources().getColor(R.color.gray_light));

        } else {
            textHeader1.setTextColor(getResources().getColor(R.color.black));
            textHeader1.setBackgroundColor(getResources().getColor(R.color.blue_light));
            textHeader2.setTextColor(getResources().getColor(R.color.black));
            textHeader2.setBackgroundColor(getResources().getColor(R.color.blue_light));
            layOut.setBackgroundColor(getResources().getColor(R.color.white));

            description.setTextColor(getResources().getColor(R.color.black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            viewSources.setBackgroundResource(R.drawable.border_yello);
            fullChapterText.setTextColor(getResources().getColor(R.color.black));
            fullChapterText.setBackgroundColor(getResources().getColor(R.color.gray_light));

            textDefination.setTextColor(getResources().getColor(R.color.background_black));
            meaningText.setTextColor(getResources().getColor(R.color.background_black));

        }

        textDefination.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        meaningText.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);
        description.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        fullChapterText.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

    }


}