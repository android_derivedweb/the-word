package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.res.ResourcesCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.theword.R;
import com.example.theword.Utils.UserSession;

public class Setting extends AppCompatActivity {

    private Spinner languageSpinner, sizeSpinner, styleSpinner;
    private String [] languages = {"English"};

    private String [] sizesName = {"Normal", "Large", "Small"};
    private String [] sizesDigit = {"18", "20", "16"};

    private String [] style = {"Helvetica", "Gill Sans Std", "Geograph", "Proforma", "Calibri", "Times New Roman"};


    private SwitchCompat darkModeSwitch;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private LinearLayout layOut;
    private TextView textHeader, txtDarkMode, txtChooseLang, txtFontColor, txtFontSize;

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));

      /*  getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        if (getActionBar() != null) {
            this.getActionBar().hide();
        }

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Setting.this);
        Log.e("CheckSessionData", session.getFontsize() + "--");

        languageSpinner = findViewById(R.id.languageSpinner);
        sizeSpinner = findViewById(R.id.sizeSpinner);
        styleSpinner = findViewById(R.id.styleSpinner);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Setting.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        layOut = findViewById(R.id.layOut);
        textHeader = findViewById(R.id.textHeader);
        txtDarkMode = findViewById(R.id.txtDarkMode);
        txtChooseLang = findViewById(R.id.txtChooseLang);
        txtFontColor = findViewById(R.id.txtFontColor);
        txtFontSize = findViewById(R.id.txtFontSize);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);


        darkModeSwitch = findViewById(R.id.darkModeSwitch);

        if (session.getIsdarkmode().equals("ON")){
            darkModeSwitch.setChecked(true);
        } else {
            darkModeSwitch.setChecked(false);
        }


        darkModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                if (b){
                    session.setIsdarkmode("ON");

                    Intent intent = new Intent(Setting.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    session.setIsdarkmode("OFF");

                    Intent intent = new Intent(Setting.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                /*
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Setting.this);
                builder1.setMessage("Need to restart the app");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                if (b){
                                    session.setIsdarkmode("ON");

                                    Intent intent = new Intent(Setting.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    session.setIsdarkmode("OFF");

                                    Intent intent = new Intent(Setting.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }

                            }
                        });

              *//*  builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });*//*

                AlertDialog alert11 = builder1.create();
                alert11.show();
*/

            }
        });



        ArrayAdapter adapter = new ArrayAdapter(Setting.this, android.R.layout.simple_spinner_item, languages);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        languageSpinner.setAdapter(adapter);


        ArrayAdapter adapterFont = new ArrayAdapter(Setting.this, android.R.layout.simple_spinner_item, sizesName);
        adapterFont.setDropDownViewResource(android.R.layout.simple_list_item_1);
        sizeSpinner.setAdapter(adapterFont);
        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                session.setFontsize(sizesDigit[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Log.e("abcd",session.getFontsize());
        if(session.getFontsize().equals("18")){
            sizeSpinner.setSelection(0);
        }else if(session.getFontsize().equals("20")){
            sizeSpinner.setSelection(1);
        }else if(session.getFontsize().equals("16")){
            sizeSpinner.setSelection(2);
        }

        ArrayAdapter adapterStyle = new ArrayAdapter(Setting.this, android.R.layout.simple_spinner_item, style);
        adapterStyle.setDropDownViewResource(android.R.layout.simple_list_item_1);
        styleSpinner.setAdapter(adapterStyle);


        if(session.getFontstyle().equals("Helvetica")){
            styleSpinner.setSelection(0);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            styleSpinner.setSelection(1);
        }else if(session.getFontstyle().equals("Geograph")){
            styleSpinner.setSelection(2);
        }else if(session.getFontstyle().equals("Proforma")){
            styleSpinner.setSelection(3);
        }else if(session.getFontstyle().equals("Calibri")){
            styleSpinner.setSelection(4);
        }else if(session.getFontstyle().equals("Times New Roman")){
            styleSpinner.setSelection(5);
        }
        styleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                session.setFontstyle(style[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        darkMode();

        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.helvetica_1), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.helvetica_1), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.helvetica_1), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.helvetica_1), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.helvetica_1), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Gill Sans Std")){

            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.gill_sans_std_2), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.gill_sans_std_2), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.gill_sans_std_2), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.gill_sans_std_2), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.gill_sans_std_2), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Geograph")){
            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.zack_3), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.zack_3), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.zack_3), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.zack_3), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.zack_3), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.proforma_4), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.proforma_4), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.proforma_4), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.proforma_4), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.proforma_4), Typeface.BOLD);

        }else if(session.getFontstyle().equals("Calibri")){

            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.calibri_5), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.calibri_5), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.calibri_5), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.calibri_5), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.calibri_5), Typeface.BOLD);


        }else if(session.getFontstyle().equals("Times New Roman")){

            textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.times_new_roman_6), Typeface.BOLD);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.times_new_roman_6), Typeface.BOLD);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.times_new_roman_6), Typeface.BOLD);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.times_new_roman_6), Typeface.BOLD);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.times_new_roman_6), Typeface.BOLD);



        }



    }



    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            layOut.setBackgroundColor(getResources().getColor(R.color.background_black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            txtDarkMode.setTextColor(getResources().getColor(R.color.blue_light));
            txtChooseLang.setTextColor(getResources().getColor(R.color.blue_light));
            txtFontColor.setTextColor(getResources().getColor(R.color.blue_light));
            txtFontSize.setTextColor(getResources().getColor(R.color.blue_light));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));
            layOut.setBackgroundColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            txtDarkMode.setTextColor(getResources().getColor(R.color.black));
            txtChooseLang.setTextColor(getResources().getColor(R.color.black));
            txtFontColor.setTextColor(getResources().getColor(R.color.black));
            txtFontSize.setTextColor(getResources().getColor(R.color.black));

        }


        txtDarkMode.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        txtChooseLang.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        txtFontColor.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        txtFontSize.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

    }


}