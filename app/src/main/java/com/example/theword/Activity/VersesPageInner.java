package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.theword.Adapter.AdapterPageInner;
import com.example.theword.Adapter.AdapterResource;
import com.example.theword.Model.VersesInnerModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VersesPageInner extends AppCompatActivity {

    private RecyclerView recList, recSources;
    private AdapterPageInner adapterPageInner;
    private AdapterResource adapterResource;
    private LinearLayout viewSources, viewSourcesOpen;
    private ImageView arrow;
    private TextView textHeader, textTop, textSource,txt_view_resources;
    private LinearLayout layOut;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private String topic_id = "";

    private UserSession session;

    private boolean isOpen = true;
    private ArrayList<VersesInnerModel> innerModelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verses_page_inner);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(VersesPageInner.this);

        topic_id = getIntent().getStringExtra("topic_id");


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VersesPageInner.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        textHeader = findViewById(R.id.textHeader);
        layOut = findViewById(R.id.layOut);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        textTop = findViewById(R.id.textTop);
        textSource = findViewById(R.id.textSource);
        txt_view_resources = findViewById(R.id.txt_view_resources);

        recList = findViewById(R.id.recList);
        recList.setLayoutManager(new LinearLayoutManager(VersesPageInner.this));
        adapterPageInner = new AdapterPageInner(VersesPageInner.this, innerModelArrayList, new AdapterPageInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                Intent intent = new Intent(VersesPageInner.this, VersesPageDetail.class);
                intent.putExtra("verse_id", innerModelArrayList.get(item).getVerse_id());
                startActivity(intent);
            }

            @Override
            public void onItemClickWishList(int item) {
                iSLike(innerModelArrayList.get(item).getVerse_id());
            }
        });
        recList.setAdapter(adapterPageInner);


        viewSourcesOpen = findViewById(R.id.viewSourcesOpen);
        viewSources = findViewById(R.id.viewSources);
        arrow = findViewById(R.id.arrow);



        recSources = findViewById(R.id.recSources);
        recSources.setLayoutManager(new LinearLayoutManager(VersesPageInner.this));
        adapterResource = new AdapterResource(VersesPageInner.this, new AdapterResource.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recSources.setAdapter(adapterResource);


        viewSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOpen) {
                    textSource.setVisibility(View.VISIBLE);
                    arrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));

                    isOpen = false;
                } else {
                    textSource.setVisibility(View.GONE);
                    arrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                    isOpen = true;
                }

            }
        });

        getData(topic_id);

        darkMode();

        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.helvetica_1), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.helvetica_1), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.helvetica_1), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.helvetica_1), Typeface.NORMAL);
            }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.gill_sans_std_2), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.zack_3), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.zack_3), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.zack_3), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.proforma_4), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.proforma_4), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.proforma_4), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.calibri_5), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.calibri_5), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.calibri_5), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.times_new_roman_6), Typeface.BOLD);
            textTop.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.times_new_roman_6), Typeface.NORMAL);
            txt_view_resources.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.times_new_roman_6), Typeface.NORMAL);
            textSource.setTypeface(ResourcesCompat.getFont(VersesPageInner.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }



    }


    private void getData(String topic_id){
        final KProgressHUD progressDialog = KProgressHUD.create(VersesPageInner.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-topic?topic_id=" + topic_id + "&user_id=" + session.getUserId(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " dd");
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONObject object = jsonObject.getJSONObject("topic");

                                    textHeader.setText(object.getString("topic"));

                                    textTop.setText(noTrailingwhiteLines(Html.fromHtml(object.getString("definition"))));

                                    textSource.setMovementMethod(LinkMovementMethod.getInstance());
                                    textSource.setText(noTrailingwhiteLines(Html.fromHtml(object.getString("view_sources"))));


                                    JSONArray jsonArray = jsonObject.getJSONArray("verses");

                                    for (int i = 0; i < jsonArray.length(); i++){
                                        JSONObject object1 = jsonArray.getJSONObject(i);

                                        VersesInnerModel versesModel = new VersesInnerModel();
                                        versesModel.setTopic_id(object1.getString("topic_id"));
                                        versesModel.setVerse_id(object1.getString("verse_id"));
                                        versesModel.setVerse(object1.getString("verse"));
                                        versesModel.setDescription(object1.getString("description"));
                                        versesModel.setMeaning_context(object1.getString("meaning_context"));
                                        versesModel.setIs_like(object1.getString("is_like"));
                                        innerModelArrayList.add(versesModel);
                                    }

                                    adapterPageInner.notifyDataSetChanged();



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(VersesPageInner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(VersesPageInner.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(VersesPageInner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(VersesPageInner.this).add(volleyMultipartRequest);
    }

    private CharSequence noTrailingwhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }


    private void iSLike(String verse_id){
        final KProgressHUD progressDialog = KProgressHUD.create(VersesPageInner.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "like-dislike-favorites?user_id=" +session.getUserId()
                        + "&verse_id=" + verse_id,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseLike",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    Toast.makeText(VersesPageInner.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(VersesPageInner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(VersesPageInner.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(VersesPageInner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(VersesPageInner.this).add(volleyMultipartRequest);
    }


    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            layOut.setBackgroundColor(getResources().getColor(R.color.background_black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            viewSourcesOpen.setBackgroundColor(getResources().getColor(R.color.background_black));
            viewSources.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_yello_bg_black));

            textSource.setTextColor(getResources().getColor(R.color.white));

            textTop.setTextColor(getResources().getColor(R.color.gray_dark_light));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));
            layOut.setBackgroundColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            viewSourcesOpen.setBackgroundColor(getResources().getColor(R.color.gray_light));
            viewSources.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_yello));

            textSource.setTextColor(getResources().getColor(R.color.black_light));

            textTop.setTextColor(getResources().getColor(R.color.background_black));

        }

        textSource.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        textTop.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

    }



}
