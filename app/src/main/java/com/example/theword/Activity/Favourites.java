package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.theword.Adapter.AdapterPageInner;
import com.example.theword.Model.VersesInnerModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;
import com.example.theword.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Favourites extends AppCompatActivity {

    private RecyclerView recList;
    private AdapterPageInner adapterPageInner;

    private LinearLayout layOut;
    private TextView textHeader, noDataText;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private ArrayList<VersesInnerModel> innerModelArrayList = new ArrayList<>();
    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Favourites.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Favourites.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        layOut = findViewById(R.id.layOut);
        textHeader = findViewById(R.id.textHeader);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        noDataText = findViewById(R.id.noDataText);


        recList = findViewById(R.id.recList);
        recList.setLayoutManager(new LinearLayoutManager(Favourites.this));
        adapterPageInner = new AdapterPageInner(Favourites.this, innerModelArrayList, new AdapterPageInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                Intent intent = new Intent(Favourites.this, VersesPageDetail.class);
                intent.putExtra("verse_id", innerModelArrayList.get(item).getVerse_id());
                startActivity(intent);
            }

            @Override
            public void onItemClickWishList(int item) {

                iSLike(innerModelArrayList.get(item).getVerse_id());
            }
        });
        recList.setAdapter(adapterPageInner);


        getFavourites();

        darkMode();

        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.helvetica_1), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.gill_sans_std_2), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Geograph")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.zack_3), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.proforma_4), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Calibri")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.calibri_5), Typeface.BOLD);
        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader.setTypeface(ResourcesCompat.getFont(Favourites.this, R.font.times_new_roman_6), Typeface.BOLD);
        }


    }


    private void getFavourites(){
        final KProgressHUD progressDialog = KProgressHUD.create(Favourites.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "get-favorites/?user_id=" + session.getUserId(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " dd");
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++){
                                        JSONObject object1 = jsonArray.getJSONObject(i);

                                        VersesInnerModel versesModel = new VersesInnerModel();
                                        versesModel.setTopic_id(object1.getString("topic_id"));
                                        versesModel.setVerse_id(object1.getString("verse_id"));
                                        versesModel.setVerse(object1.getString("verse"));
                                        versesModel.setDescription(object1.getString("description"));
                                        versesModel.setMeaning_context(object1.getString("meaning_context"));
                                        versesModel.setIs_like("1");
                                        innerModelArrayList.add(versesModel);
                                    }

                                    adapterPageInner.notifyDataSetChanged();



                                    if (innerModelArrayList.isEmpty()){
                                        noDataText.setVisibility(View.VISIBLE);
                                    } else {
                                        noDataText.setVisibility(View.GONE);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(Favourites.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Favourites.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Favourites.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Favourites.this).add(volleyMultipartRequest);
    }


    private void iSLike(String verse_id){
        final KProgressHUD progressDialog = KProgressHUD.create(Favourites.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "like-dislike-favorites?user_id=" + session.getUserId()
                        + "&verse_id=" + verse_id,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseLike",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    Toast.makeText(Favourites.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(Favourites.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){

                                session.logout();
                                Intent intent = new Intent(Favourites.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Favourites.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //     params.put("search", search);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Favourites.this).add(volleyMultipartRequest);
    }


    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            layOut.setBackgroundColor(getResources().getColor(R.color.background_black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));
            layOut.setBackgroundColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

        }

    }

}