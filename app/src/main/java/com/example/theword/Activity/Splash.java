package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.theword.R;
import com.example.theword.Utils.UserSession;

public class Splash extends AppCompatActivity {

    private LinearLayout splashImage;
    private ImageView splash_image, logo_image;

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Splash.this);

        splashImage = findViewById(R.id.splashImage);
        splash_image = findViewById(R.id.splash_image);
        logo_image = findViewById(R.id.logo_image);


        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);



        forDarkMode();

    }


    @SuppressLint("UseCompatLoadingForDrawables")
    public void forDarkMode(){

        if (session.getIsdarkmode().equals("ON")){
      //      splashImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.splash_dark));
            splashImage.setBackgroundColor(getResources().getColor(R.color.background_black));
            splash_image.setImageDrawable(getResources().getDrawable(R.drawable.icon_splash_dark));
            logo_image.setImageDrawable(getResources().getDrawable(R.drawable.logo_splash_dark));
        } else {
      //      splashImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.splash_light));
            splashImage.setBackgroundColor(getResources().getColor(R.color.white));
            splash_image.setImageDrawable(getResources().getDrawable(R.drawable.icon_splash_light));
            logo_image.setImageDrawable(getResources().getDrawable(R.drawable.logo_splash_light));
        }

    }

}