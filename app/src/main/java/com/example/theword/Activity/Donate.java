package com.example.theword.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.theword.R;
import com.example.theword.Utils.UserSession;

public class Donate extends AppCompatActivity {

    private EditText etDonateReason;
    private TextView textHeader, helpText, txtPaypal, txtCreditCard, donatTxt, paypalText;
    private LinearLayout layOut;

    //bottom bar
    private RelativeLayout bottomBar;
    private ImageView wordImage;

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.gray_light));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Donate.this);


        etDonateReason = findViewById(R.id.etDonateReason);
        txtPaypal = findViewById(R.id.txtPaypal);
        txtCreditCard = findViewById(R.id.txtCreditCard);
        textHeader = findViewById(R.id.textHeader);
        helpText = findViewById(R.id.helpText);
        layOut = findViewById(R.id.layOut);
        bottomBar = findViewById(R.id.bottomBar);
        wordImage = findViewById(R.id.wordImage);
        donatTxt = findViewById(R.id.donatTxt);
        paypalText = findViewById(R.id.paypalText);


        paypalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "paypal.me/iskycreative"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                startActivity(intent);*/

                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.paypal.com/paypalme/iskycreative"));
                startActivity(intent);
            }
        });


        donatTxt.setText(Html.fromHtml("<p><em>Go, therefore, and&nbsp;make disciples of&nbsp;all the nations,&nbsp;baptizing them in the name of the Father and the Son and the Holy Spirit&hellip; - Matthew 28:19</em></p>\n" +
                "<p>All help received will go solely towards developing and growing our App.</p>\n" +
                "<p>Help us grow and make our App known around the world for those who are thirsty for &ldquo;The Word&rdquo; of God.</p>\n" +
                "<p>We plan to increase the reach of The Word App by translating it into as many different languages as possible, and to promote it through different media platforms.</p>\n" +
                "<p>We will also continue to increase our content by adding more topics periodically in order to leave no topic or Bible verse untouched.</p>\n" +
                "Thank you for your blessings and support."));


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.homeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Donate.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        darkMode();


        if(session.getFontstyle().equals("Helvetica")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.helvetica_1), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Gill Sans Std")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.gill_sans_std_2), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Geograph")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.zack_3), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Proforma")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.proforma_4), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Calibri")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.calibri_5), Typeface.NORMAL);

        }else if(session.getFontstyle().equals("Times New Roman")){
            textHeader.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.BOLD);
            helpText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.NORMAL);
            txtPaypal.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.BOLD);
            txtCreditCard.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.BOLD);
            donatTxt.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.NORMAL);
            paypalText.setTypeface(ResourcesCompat.getFont(Donate.this, R.font.times_new_roman_6), Typeface.NORMAL);
        }


    }


    private void darkMode(){

        if (session.getIsdarkmode().equals("ON")) {
            textHeader.setTextColor(getResources().getColor(R.color.gray_light));
            textHeader.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            helpText.setTextColor(getResources().getColor(R.color.gray_light));
            layOut.setBackgroundColor(getResources().getColor(R.color.background_black));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.black_for_darkmode));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_light));

            etDonateReason.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            etDonateReason.setTextColor(getResources().getColor(R.color.gray_dark_light));
            etDonateReason.setHintTextColor(getResources().getColor(R.color.gray_dark_light));

            txtPaypal.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            txtPaypal.setTextColor(getResources().getColor(R.color.gray_dark_light));

            txtCreditCard.setBackground(getResources().getDrawable(R.drawable.border_gray_backg_black));
            txtCreditCard.setTextColor(getResources().getColor(R.color.gray_dark_light));

            donatTxt.setTextColor(getResources().getColor(R.color.white));
            donatTxt.setBackgroundColor(getResources().getColor(R.color.background_black));

        } else {
            textHeader.setTextColor(getResources().getColor(R.color.black));
            textHeader.setBackgroundColor(getResources().getColor(R.color.blue_light));
            helpText.setTextColor(getResources().getColor(R.color.black));
            layOut.setBackgroundColor(getResources().getColor(R.color.white));

            bottomBar.setBackgroundColor(getResources().getColor(R.color.gray_light));
            wordImage.setImageDrawable(getResources().getDrawable(R.drawable.word_logo_dark));

            etDonateReason.setBackground(getResources().getDrawable(R.drawable.border_black_round));
            etDonateReason.setTextColor(getResources().getColor(R.color.black));
            etDonateReason.setHintTextColor(getResources().getColor(R.color.black));

            txtPaypal.setBackground(getResources().getDrawable(R.drawable.border_black_round));
            txtPaypal.setTextColor(getResources().getColor(R.color.black));

            txtCreditCard.setBackground(getResources().getDrawable(R.drawable.border_black_round));
            txtCreditCard.setTextColor(getResources().getColor(R.color.black));

            donatTxt.setTextColor(getResources().getColor(R.color.black));
            donatTxt.setBackgroundColor(getResources().getColor(R.color.white));
        }

        etDonateReason.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);
        txtPaypal.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);
        txtCreditCard.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);
        helpText.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) + 2);

        donatTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

    }


}