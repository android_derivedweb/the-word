package com.example.theword.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theword.R;
import com.example.theword.Utils.UserSession;


public class AdapterResource extends RecyclerView.Adapter<AdapterResource.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private UserSession session;


    public AdapterResource(Context mContext, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        session = new UserSession(mContext);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_resource, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) - 1);

        if(session.getFontstyle().equals("Helvetica")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.times_new_roman_6), Typeface.NORMAL);
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView text;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
