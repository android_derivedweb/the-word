package com.example.theword.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theword.Activity.Favourites;
import com.example.theword.Model.VersesInnerModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;

import java.util.ArrayList;


public class AdapterPageInner extends RecyclerView.Adapter<AdapterPageInner.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private UserSession session;
    private ArrayList<VersesInnerModel> innerModelArrayList;

    public AdapterPageInner(Context mContext, ArrayList<VersesInnerModel> innerModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.innerModelArrayList = innerModelArrayList;
        session = new UserSession(mContext);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_page_inner, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.mean_context.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.header.setText(noTrailingwhiteLines(Html.fromHtml(innerModelArrayList.get(position).getVerse())));

        holder.desCription.setText(noTrailingwhiteLines(Html.fromHtml(innerModelArrayList.get(position).getDescription())));


        if (innerModelArrayList.get(position).getIs_like().equals("1")){
            holder.favourite.setBackgroundResource(R.drawable.heart_fill);
        } else {
            holder.favourite.setBackgroundResource(R.drawable.heart);
        }

        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (innerModelArrayList.get(position).getIs_like().equals("1")){
                    holder.favourite.setBackgroundResource(R.drawable.heart);
                    innerModelArrayList.get(position).setIs_like("0");
                } else {
                    holder.favourite.setBackgroundResource(R.drawable.heart_fill);
                    innerModelArrayList.get(position).setIs_like("1");
                }

                listener.onItemClickWishList(position);

            }
        });

        if (session.getIsdarkmode().equals("ON")) {
            holder.layOut.setBackgroundColor(mContext.getResources().getColor(R.color.background_black));
            holder.desCription.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.mean_context.setImageDrawable(mContext.getResources().getDrawable(R.drawable.mean_con_dark));

            holder.header.setTextColor(mContext.getResources().getColor(R.color.gray_light));
        } else {
            holder.layOut.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.desCription.setTextColor(mContext.getResources().getColor(R.color.black));
            holder.mean_context.setImageDrawable(mContext.getResources().getDrawable(R.drawable.mean_con_light));

            holder.header.setTextColor(mContext.getResources().getColor(R.color.background_black));

        }


        holder.header.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));
        holder.desCription.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()) - 1);


        if(session.getFontstyle().equals("Helvetica")){
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.helvetica_1), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.gill_sans_std_2), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){
           /* textHeader.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.geograph_3), Typeface.NORMAL);
            txtDarkMode.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.geograph_3), Typeface.NORMAL);
            txtChooseLang.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.geograph_3), Typeface.NORMAL);
            txtFontColor.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.geograph_3), Typeface.NORMAL);
            txtFontSize.setTypeface(ResourcesCompat.getFont(Setting.this, R.font.geograph_3), Typeface.NORMAL);
*/
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.zack_3), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.proforma_4), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.calibri_5), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            holder.header.setTypeface(ResourcesCompat.getFont(mContext, R.font.times_new_roman_6), Typeface.BOLD);
            holder.desCription.setTypeface(ResourcesCompat.getFont(mContext, R.font.times_new_roman_6), Typeface.NORMAL);
        }

    }

    @Override
    public int getItemCount() {
        return innerModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView favourite, mean_context;
        LinearLayout layOut;
        TextView desCription, header;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            favourite = itemView.findViewById(R.id.favourite);
            layOut = itemView.findViewById(R.id.layOut);
            desCription = itemView.findViewById(R.id.desCription);
            header = itemView.findViewById(R.id.header);
            mean_context = itemView.findViewById(R.id.mean_context);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickWishList(int item);
    }

    private CharSequence noTrailingwhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }



}
