package com.example.theword.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theword.Model.VersesModel;
import com.example.theword.R;
import com.example.theword.Utils.UserSession;

import java.util.ArrayList;


public class AdapterTopics extends RecyclerView.Adapter<AdapterTopics.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private UserSession session;
    private ArrayList<VersesModel> versesModelArrayList;


    public AdapterTopics(Context mContext, ArrayList<VersesModel> versesModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.versesModelArrayList = versesModelArrayList;

        session = new UserSession(mContext);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_topics, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.text.setText(versesModelArrayList.get(position).getTopic());


        if (session.getIsdarkmode().equals("ON")){
            holder.text.setTextColor(mContext.getResources().getColor(R.color.gray_light));
            holder.layOut.setBackgroundColor(mContext.getResources().getColor(R.color.background_black));
        } else {
            holder.text.setTextColor(mContext.getResources().getColor(R.color.black_light));
            holder.layOut.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP,Float.parseFloat(session.getFontsize()));

        if(session.getFontstyle().equals("Helvetica")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.helvetica_1), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Gill Sans Std")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.gill_sans_std_2), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Geograph")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.zack_3), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Proforma")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.proforma_4), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Calibri")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.calibri_5), Typeface.NORMAL);
        }else if(session.getFontstyle().equals("Times New Roman")){
            holder.text.setTypeface(ResourcesCompat.getFont(mContext, R.font.times_new_roman_6), Typeface.NORMAL);
        }
    }

    @Override
    public int getItemCount() {
        return versesModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView text;
        LinearLayout layOut;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text);
            layOut = itemView.findViewById(R.id.layOut);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


    public void updateList(ArrayList<VersesModel> list){
        this.versesModelArrayList = list;
        notifyDataSetChanged();
    }


}
