package com.example.theword.Model;

public class VersesModel {

    String topic_id;
    String topic;
    String topic_small_case;

    public String getTopic_small_case() {
        return topic_small_case;
    }

    public void setTopic_small_case(String topic_small_case) {
        this.topic_small_case = topic_small_case;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
