package com.example.theword.Model;

public class VersesInnerModel {

    String verse_id;
    String topic_id;
    String verse;
    String description;
    String meaning_context;
    String is_like;

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public String getVerse_id() {
        return verse_id;
    }

    public void setVerse_id(String verse_id) {
        this.verse_id = verse_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getVerse() {
        return verse;
    }

    public void setVerse(String verse) {
        this.verse = verse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeaning_context() {
        return meaning_context;
    }

    public void setMeaning_context(String meaning_context) {
        this.meaning_context = meaning_context;
    }
}
