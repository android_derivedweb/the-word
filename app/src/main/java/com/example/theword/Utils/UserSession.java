package com.example.theword.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseUser;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public static String BASEURL = "https://www.the-word.me/the_word_admin/api/";

    private static final String PREF_NAME = "UserSessionPref";


    private static final String ISDARKMODE = "IsDarkMode";
    private static final String FONTSIZE = "FontSize";
    private static final String FONTSTYLE = "FONTSTYLE";
    private static final String FIREBASE_DEVICE_TOKEN = "firebase_device_token";
    private static final String USER_ID = "USER_ID";


    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public void setFirbaseDeviceToken(String firbaseDeviceToken) {
        editor.putString(FIREBASE_DEVICE_TOKEN, firbaseDeviceToken);
        editor.commit();
    }


    public String getFirbaseDeviceToken() {
        return sharedPreferences.getString(FIREBASE_DEVICE_TOKEN, "");
    }

    public void setUserId(String userId) {
        editor.putString(USER_ID, userId);
        editor.commit();
    }


    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "");
    }


    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }


    public String getIsdarkmode() {
        return sharedPreferences.getString(ISDARKMODE, "OFF");
    }

    public void setIsdarkmode(String isdarkmode) {
        editor.putString(ISDARKMODE, isdarkmode);
        editor.commit();
    }

    public String getFontsize() {
        return sharedPreferences.getString(FONTSIZE, "14");
    }
    public String getFontstyle() {
        return sharedPreferences.getString(FONTSTYLE, "Helvetica");
    }

    public void setFontsize(String isdarkmode) {
        editor.putString(FONTSIZE, isdarkmode);
        editor.commit();
    }

    public void setFontstyle(String isdarkmode) {
        editor.putString(FONTSTYLE, isdarkmode);
        editor.commit();
    }


}
